const express = require('express')
const app = express()
//const path = require("path")
const logger = require ('./middleware/logger')
const PORT = process.env.PORT || 3000

// Set static folder (just for test, not use here)
// app.use(express.static(path.join(__dirname, 'public')))


// Init middleware
//app.use(logger)

// Body Parser Middleware
app.use(express.json())
// Handle form submissions
app.use(express.urlencoded({extended: false})) // handle URL encoded data


// Members API Routes
app.use('/api/members', require('./routes/api/members'))

app.listen(PORT, () => console.log(`Server running on port ${PORT}`))