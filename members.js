const members = [
    {
        id: 1,
        name: 'Nicolas Duda',
        email: 'nicolas@duda.fr',
        status: 'active'
    },
    {
        id: 2,
        name: 'John Doe',
        email: 'john@doe.fr',
        status: 'inactive'
    },
    {
        id: 3,
        name: 'Bob Williams',
        email: 'bob@williams.fr',
        status: 'active'
    }
]

module.exports = members;